/*
    Copyright (C) 2020 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "universe/CSVObjects.hpp"

#include "physics/blackbody.hpp"
#include <cfloat>

std::unique_ptr<GLTexture>& CSVObjects::starTex()
{
	static std::unique_ptr<GLTexture> starTex;
	return starTex;
}

std::unique_ptr<GLTexture>& CSVObjects::galTex()
{
	static std::unique_ptr<GLTexture> galTex;
	return galTex;
}

CSVObjects::CSVObjects(QJsonObject const& json, bool galaxies)
    : shader(galaxies ? "galaxies" : "stars")
    , galaxies(galaxies)
    , conShader("constellations")
{
	if(!galaxies
	   && QFile::exists(QSettings().value("data/rootdir").toString()
	                    + json["confile"].toString()))
	{
		initWithConstellations(QSettings().value("data/rootdir").toString()
		                           + json["file"].toString(),
		                       QSettings().value("data/rootdir").toString()
		                           + json["confile"].toString());
	}
	else
	{
		init(QSettings().value("data/rootdir").toString()
		         + json["file"].toString(),
		     QSettings().value("data/rootdir").toString()
		         + json["atlasfile"].toString());
	}
}

void CSVObjects::init(QString const& csvFile, QString const& atlasFile)
{
	if(starTex() == nullptr)
	{
		starTex() = std::make_unique<GLTexture>(
		    utils::getAbsoluteDataPath("images/star.png").toLatin1().data());
		starTex()->generateMipmap();
	}
	if(galTex() == nullptr && !atlasFile.isEmpty())
	{
		galTex() = std::make_unique<GLTexture>(atlasFile.toLatin1().data());
		galTex()->generateMipmap();
	}
	QFile file(csvFile);
	if(file.open(QFile::ReadOnly))
	{
		QString line(file.readLine().data());

		// parse first line for columns numbers
		const QList<QString> columns
		    = {"names",   "x",       "y",       "z",  "absmag",
		       "rabsmag", "gabsmag", "babsmag", "ci", "teff"};
		std::map<QString, int> columnsNumbers;

		for(int i(0); i < line.split(",").size(); ++i)
		{
			const QString str(line.split(",")[i]);
			if(columns.contains(str.simplified()))
			{
				columnsNumbers[str.simplified()] = i;
			}
		}

		line = file.readLine().data();
		while(line != "")
		{
			objects.push_back(parseLine(line, columnsNumbers));
			for(auto const& starName : objects[objects.size() - 1].names)
			{
				indexByName[starName.name] = objects.size() - 1;
			}
			line = file.readLine().data();
		}
		file.close();
	}

	std::vector<float> vertices;
	for(auto object : objects)
	{
		vertices.push_back(object.x);
		vertices.push_back(object.y);
		vertices.push_back(object.z);
		vertices.push_back(object.absmag);
		vertices.push_back(object.color.redF());
		vertices.push_back(object.color.greenF());
		vertices.push_back(object.color.blueF());

		bbox.minx = std::min(bbox.minx, static_cast<float>(object.x));
		bbox.miny = std::min(bbox.miny, static_cast<float>(object.y));
		bbox.minz = std::min(bbox.minz, static_cast<float>(object.z));

		bbox.maxx = std::max(bbox.maxx, static_cast<float>(object.x));
		bbox.maxy = std::max(bbox.maxy, static_cast<float>(object.y));
		bbox.maxz = std::max(bbox.maxz, static_cast<float>(object.z));
	}
	bbox.mid = QVector3D((bbox.maxx + bbox.minx) * 0.5f,
	                     (bbox.maxy + bbox.miny) * 0.5f,
	                     (bbox.maxz + bbox.minz) * 0.5f);
	bbox.diameter
	    = sqrt(pow(bbox.maxx - bbox.minx, 2) + pow(bbox.maxy - bbox.miny, 2)
	           + pow(bbox.maxz - bbox.minz, 2));

	mesh.setVertexShaderMapping(shader,
	                            {{"position", 3}, {"absmag", 1}, {"color", 3}});
	mesh.setVertices(vertices);
}

void CSVObjects::initWithConstellations(QString const& csvFile,
                                        QString const& constellationsFile)
{
	init(csvFile, "");
	containsConstellations = true;
	conShader.setUniform("color", QColor(0, 128, 255));

	std::vector<float> vertices;
	std::vector<unsigned int> elements;

	QFile file(constellationsFile);
	if(file.open(QFile::ReadOnly))
	{
		QString line(file.readLine().data());
		QString currentName = "";
		Vector3 currentPosSum;
		unsigned int posNumber = 0;
		while(line != "")
		{
			if(line[0] != '\t')
			{
				if(posNumber > 0)
				{
					// average position
					currentPosSum /= posNumber;
					conLabels.emplace_back(
					    currentPosSum,
					    LabelRenderer{currentName, QColor(0, 230, 255), false});
				}
				currentName = line.simplified();
				currentName.replace(QString{"\\n"}, QString{'\n'});
				currentPosSum = Vector3();
				posNumber     = 0;
				line          = file.readLine().data();
				continue;
			}

			auto starsNames = line.simplified().split(',');

			for(auto const& starName : starsNames)
			{
				int index(-1);
				if(indexByName.contains(starName))
				{
					index = indexByName[starName];
				}

				if(index == -1)
				{
					// print not found stars
					// qDebug() << starName;
					continue;
				}

				Object object(objects[index]);
				elements.push_back(vertices.size() / 3);
				vertices.push_back(object.x);
				vertices.push_back(object.y);
				vertices.push_back(object.z);

				currentPosSum += Vector3(object.x, object.y, object.z);
				++posNumber;
			}
			elements.push_back(0xFFFF);

			line = file.readLine().data();
		}
		if(posNumber > 0)
		{
			// average position
			currentPosSum /= posNumber;
			conLabels.emplace_back(
			    currentPosSum,
			    LabelRenderer{currentName, QColor(0, 230, 255), false});
		}
		file.close();
	}

	conMesh.setVertexShaderMapping(conShader, {{"position", 3}});
	conMesh.setVertices(vertices, elements);
}

BBox CSVObjects::getBoundingBox() const
{
	return bbox;
}

void CSVObjects::render(Camera const& camera, ToneMappingModel const& tmm)
{
	QMatrix4x4 model;
	QVector3D campos;
	getModelAndCampos(camera, model, campos);

	{
		const GLStateSet glState(
		    {{GL_CLIP_DISTANCE0, true}, {GL_PROGRAM_POINT_SIZE, true}});
		const GLBlendSet glBlend({GL_ONE, GL_ONE});
		shader.setUniform("pixelSolidAngle", camera.pixelSolidAngle());
		auto vis = getVisibility();
		if(useBrightnessMultiplier())
		{
			vis *= brightnessMultiplier;
		}
		shader.setUniform("brightnessMultiplier", vis);
		shader.setUniform("campos", campos);
		if(galaxies)
		{
			shader.setUniform("atlassize", QVector2D(47, 10));
			shader.setUniform("colormix", colormix);
		}
		else
		{
			shader.setUniform("camexp", tmm.exposure);
			shader.setUniform("camdynrange", tmm.dynamicrange);
		}
		GLHandler::useTextures({galaxies ? galTex().get() : starTex().get()});
		GLHandler::setUpRender(shader, model);
		mesh.render();
	}

	if(containsConstellations
	   && (constellationsAlpha > 0.f || constellationsLabels > 0.f))
	{
		const GLStateSet glState({{GL_MULTISAMPLE, true}});
		{
			const GLStateSet glState(
			    {{GL_LINE_SMOOTH, true}, {GL_PRIMITIVE_RESTART, true}});
			const GLBlendSet glBlend({GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA});
			GLHandler::glf().glLineWidth(2.f);
			GLHandler::glf().glPrimitiveRestartIndex(0xFFFF);

			conShader.setUniform("alpha", constellationsAlpha);
			conShader.setUniform("exposure", tmm.exposure);
			conShader.setUniform("dynamicrange", tmm.dynamicrange);
			conShader.setUniform("camPos", Utils::toQt(camera.position));
			conShader.setUniform("unit", static_cast<float>(unit));

			GLHandler::setUpRender(conShader, model);
			conMesh.render(PrimitiveType::LINE_STRIP);

			GLHandler::glf().glLineWidth(1.f);
		}

		if(constellationsLabels > 0.f)
		{
			float coeff(1.0 - 17.0 * camera.position.length());
			if(coeff < 0.f)
			{
				coeff = 0.f;
			}
			coeff = pow(coeff, 0.5f);
			for(auto& conLabel : conLabels)
			{
				const Vector3 pos(conLabel.first);
				const Vector3 camRelPos(Utils::fromQt(campos) - pos);
				const Vector3 unitRelPos(camRelPos.getUnitForm());

				// orient label towards camera and place before 8000
				const float yaw(atan2(unitRelPos[1], unitRelPos[0]));
				const float pitch(-1.0 * asin(unitRelPos[2]));
				const double rescale(camRelPos.length() <= 8000.0
				                         ? 1.0
				                         : 8000.0 / camRelPos.length());

				QMatrix4x4 model2;
				model2.translate(rescale * Utils::toQt(pos));
				model2.rotate(yaw * 180.f / M_PI + 90.f, 0.0, 0.0, 1.0);
				model2.rotate(pitch * 180.f / M_PI + 90.f, 1.0, 0.0, 0.0);
				model2.scale(rescale * camRelPos.length() / 3.0);
				conLabel.second.updateModel(model * model2);

				conLabel.second.setAlpha(constellationsLabels * coeff);
				conLabel.second.render(tmm.exposure, tmm.dynamicrange);
			}
		}
	}
}

QColor CSVObjects::colorFromColorIndex(float ci)
{
	// https://stackoverflow.com/questions/21977786/star-b-v-color-index-to-apparent-rgb-color
	const float temperature
	    = 4600
	      * ((1.f / ((0.92f * ci) + 1.7f)) + (1.f / ((0.92f * ci) + 0.62f)));

	return GLHandler::sRGBToLinear(
	    Utils::toQt(blackbody::colorFromTemperature(temperature)));
}

CSVObjects::Object
    CSVObjects::parseLine(QString const& line,
                          std::map<QString, int> const& columnsNumbers)
{
	Object result;
	const QStringList splitted(line.split(","));

	if(columnsNumbers.find("names") != columnsNumbers.cend())
	{
		const QString names = splitted[columnsNumbers.at("names")].simplified();
		if(names != "")
		{
			const QStringList namesList = names.split('|');
			for(auto const& richName : namesList)
			{
				const QStringList splittedName = richName.split(':');
				QString const& name(splittedName[0]);
				Designation desig = Designation::UNKNOWN;
				if(splittedName.size() > 1)
				{
					QString const& desigStr(splittedName[1]);
					if(desigStr == "proper")
					{
						desig = Designation::PROPER;
					}
					else if(desigStr == "bayer")
					{
						desig = Designation::BAYER;
					}
					else if(desigStr == "flam")
					{
						desig = Designation::FLAMSTEED;
					}
				}
				result.names.push_back({desig, name});
			}
		}
	}

	result.x = splitted[columnsNumbers.at("x")].simplified().toDouble();
	result.y = splitted[columnsNumbers.at("y")].simplified().toDouble();
	result.z = splitted[columnsNumbers.at("z")].simplified().toDouble();

	if(columnsNumbers.find("absmag") != columnsNumbers.cend())
	{
		result.absmag
		    = splitted[columnsNumbers.at("absmag")].simplified().toDouble();
		if(columnsNumbers.find("ci") != columnsNumbers.cend())
		{
			result.color = colorFromColorIndex(
			    splitted[columnsNumbers.at("ci")].simplified().toDouble());
		}
		else
		{
			result.color = GLHandler::sRGBToLinear(
			    Utils::toQt(blackbody::colorFromTemperature(
			        splitted[columnsNumbers.at("teff")]
			            .simplified()
			            .toDouble())));
		}
	}
	else
	{
		double rabsmag
		    = splitted[columnsNumbers.at("rabsmag")].simplified().toDouble();
		double gabsmag
		    = splitted[columnsNumbers.at("gabsmag")].simplified().toDouble();
		double babsmag
		    = splitted[columnsNumbers.at("babsmag")].simplified().toDouble();

		// invalid magnitudes
		if(rabsmag < -90.0)
		{
			rabsmag *= -1.0;
		}
		if(gabsmag < -90.0)
		{
			gabsmag *= -1.0;
		}
		if(babsmag < -90.0)
		{
			babsmag *= -1.0;
		}

		const double r = Color::illuminanceFromMag(rabsmag);
		const double g = Color::illuminanceFromMag(gabsmag);
		const double b = Color::illuminanceFromMag(babsmag);
		result.absmag  = Color::magFromIlluminance(r + g + b);

		const auto xyY = Color::rgbtoxyY(r, g, b);
		const auto rgb = Color::xyYtorgb(xyY[0], xyY[1], 1.0);
		result.color   = QColor::fromRgbF(rgb[0], rgb[1], rgb[2], 1.0);
	}

	return result;
}

CSVObjects::~CSVObjects()
{
	// TODO don't do that, check if last instance of CSVObjects !
	if(starTex() != nullptr && !galaxies)
	{
		starTex().reset();
	}
	if(galTex() != nullptr && galaxies)
	{
		galTex().reset();
	}
}

QList<QPair<QString, QWidget*>>
    CSVObjects::getStarsLauncherFields(QWidget& parent, QJsonObject& jsonObj)
{
	QList<QPair<QString, QWidget*>> result;

	auto* pathSelector
	    = make_qt_unique<PathSelector>(parent, QObject::tr("CSV path"));
	QObject::connect(pathSelector, &PathSelector::pathChanged,
	                 [&jsonObj](QString const& path)
	                 { jsonObj["file"] = path; });
	pathSelector->setPath(jsonObj["file"].toString());

	result.append({QObject::tr("CSV Path:"), pathSelector});

	pathSelector = make_qt_unique<PathSelector>(
	    parent, QObject::tr("Constellations path"));
	QObject::connect(pathSelector, &PathSelector::pathChanged,
	                 [&jsonObj](QString const& path)
	                 { jsonObj["confile"] = path; });
	pathSelector->setPath(jsonObj["confile"].toString());

	result.append({QObject::tr("Consellations Path:"), pathSelector});

	return result;
}

QList<QPair<QString, QWidget*>>
    CSVObjects::getGalaxiesLauncherFields(QWidget& parent, QJsonObject& jsonObj)
{
	QList<QPair<QString, QWidget*>> result;

	auto* pathSelector
	    = make_qt_unique<PathSelector>(parent, QObject::tr("CSV path"));
	QObject::connect(pathSelector, &PathSelector::pathChanged,
	                 [&jsonObj](QString const& path)
	                 { jsonObj["file"] = path; });
	pathSelector->setPath(jsonObj["file"].toString());

	result.append({QObject::tr("CSV Path:"), pathSelector});

	pathSelector
	    = make_qt_unique<PathSelector>(parent, QObject::tr("Atlas path"));
	QObject::connect(pathSelector, &PathSelector::pathChanged,
	                 [&jsonObj](QString const& path)
	                 { jsonObj["atlasfile"] = path; });
	pathSelector->setPath(jsonObj["atlasfile"].toString());

	result.append({QObject::tr("Atlas Path:"), pathSelector});

	return result;
}

QString CSVObjects::desigToStr(Designation desig)
{
	switch(desig)
	{
		case Designation::UNKNOWN:
			return QObject::tr("Unknown");
		case Designation::PROPER:
			return QObject::tr("Proper name");
		case Designation::BAYER:
			return QObject::tr("Bayer");
		case Designation::FLAMSTEED:
			return QObject::tr("Flamsteed");
	}
	return "";
}
